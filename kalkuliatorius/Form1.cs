﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace kalkuliatorius
{
    public partial class Form1 : Form
    {
        private string x1="";
        private string operacija="";
        

        public Form1()
        {
            InitializeComponent();
        }
        private void RasykIEkrana(string skaicius)
        {
            if (ekranas.Text.Length == 1 && ekranas.Text[0] == '0')
            {
                ekranas.Clear();
            }
            if (ekranas.Text.Length < 8)
            {
                ekranas.Text = ekranas.Text + skaicius;
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            RasykIEkrana("0");

        }
        private void button1_Click(object sender, EventArgs e)
        {
            RasykIEkrana("1");

            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            RasykIEkrana("2");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            RasykIEkrana("3");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            RasykIEkrana("4");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            RasykIEkrana("5");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            RasykIEkrana("6");
        }

        private void button9_Click(object sender, EventArgs e)
        {

            RasykIEkrana("7");
        }

        private void button8_Click(object sender, EventArgs e)
        {
            RasykIEkrana("8");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            RasykIEkrana("9");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "+";
            
        }


        private void button13_Click_1(object sender, EventArgs e)
        {
            if(x1.Length==0||ekranas.Text.Length==0)
            {
                return;
            }
            double skaicius1 = Convert.ToDouble (x1);
            double skaicius2 = Convert.ToDouble(ekranas.Text);

            double skaicius3=0;

            if (operacija == "+")
            {
                skaicius3 = skaicius1 + skaicius2;
            }
            else if (operacija == "-")
            {
                skaicius3 = skaicius1 - skaicius2;
            }
            else if (operacija == "/")
            {
                skaicius3 = skaicius1 / skaicius2;
            }
            else if (operacija == "*")
            {
                skaicius3 = skaicius1 * skaicius2;
            }

            ekranas.Text = skaicius3.ToString();
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            {
                x1 = ekranas.Text;
                ekranas.Text = "";
                operacija = "-";
                
            }

        }

        private void button14_Click(object sender, EventArgs e)
        {
            {
                x1 = ekranas.Text;
                ekranas.Text = "";
                operacija = "/";
               
            }

        }

        private void button15_Click(object sender, EventArgs e)
        {
            x1 = ekranas.Text;
            ekranas.Text = "";
            operacija = "*";

        }

        private void isvalymas_Click(object sender, EventArgs e)
        {
            ekranas.Text = "";
        }

        private void button16_Click(object sender, EventArgs e)
        {
            double skaicius2 = Convert.ToDouble(ekranas.Text);
            skaicius2 = skaicius2 * (-1);
            ekranas.Text = skaicius2.ToString();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            if (ekranas.Text.Contains("."))
            {
                return;
            }

            ekranas.Text = ekranas.Text + ".";

            
        }
    }
}
